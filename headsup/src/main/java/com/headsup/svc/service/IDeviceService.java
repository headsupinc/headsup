package com.headsup.svc.service;

import java.util.List;

import com.headsup.svc.Device;
import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.geom.Geometry;

public interface IDeviceService {

	public Device getDevice(final Long id);
	
	public Device getDevice(final String phoneNumber, final String imei);
	
	public void update(final Device device);

	/*
	 * as of 7/30/2015 DB function 'dwithin' not supported by this dialect
	 *
	 *public List<Device> getWithinDistanceOf(final Device device, final Double radius);
	*/
	
	public List<Device> getWithinEnvelope(final Device device, final Envelope envelope, final int SRID);
	
	public List<Device> getContains(final Device device, final Geometry geometry);
	
	public List<Device> getWithin(final Device device, final Geometry geometry);	
}
