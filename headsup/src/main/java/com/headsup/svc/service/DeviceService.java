package com.headsup.svc.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.headsup.svc.Device;
import com.headsup.svc.repository.IDeviceRepository;
import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.geom.Geometry;

@Service
@Transactional
public class DeviceService implements IDeviceService {

	private static final Logger logger = LoggerFactory.getLogger(DeviceService.class);
	
	@Autowired
	private IDeviceRepository deviceRepository;
	
	public Device getDevice(final Long id) {
		return deviceRepository.getDevice(id);
	}
	
	public Device getDevice(String phoneNumber, final String imei) {
		return deviceRepository.getDevice(phoneNumber, imei);
	}
	
	public void update(Device device) {
		deviceRepository.update(device);
	}
	
	/*
	 *
	 * as of 7/30/2015 DB function 'dwithin' not supported by this dialect
	 * 
	public List<Device> getWithinDistanceOf(final Device device, final Double radius) {
		return deviceRepository.getWithinDistanceOf(device, radius);
	}
	*/
	
	public List<Device> getWithinEnvelope(final Device device, final Envelope envelope, final int SRID) {
		return deviceRepository.getWithinEnvelope(device, envelope, SRID);
	}
	
	public List<Device> getContains(final Device device, final Geometry geometry) {
		return deviceRepository.getContains(device, geometry);
	}
	
	public List<Device> getWithin(final Device device, final Geometry geometry) {
		return deviceRepository.getWithin(device, geometry);
	}
}
