package com.headsup.svc.resource;

import java.util.List;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.headsup.svc.Device;

public class DevicesResource extends ResourceSupport {
	
	@JsonProperty("devices")
	private final List<Device> devices;
	
	public DevicesResource(final List<Device> devices) {
		this.devices = devices;
	}
	
	public List<Device> getDevices() {
		return devices;
	}
}
