package com.headsup.svc.resource;

import org.springframework.hateoas.ResourceSupport;

import com.headsup.svc.Device;

public class DeviceResource extends ResourceSupport {
	
	private final Device device;
	
	public DeviceResource(final Device device) {
		this.device = device;
	}
	
	public Device getDevice() {
		return device;
	}
}
