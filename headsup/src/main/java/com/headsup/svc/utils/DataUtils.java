package com.headsup.svc.utils;

import java.util.ArrayList;
import java.util.List;

import com.headsup.svc.Device;
import com.vividsolutions.jts.geom.Point;

public class DataUtils {

	public static List<Device> mockDevices() {
		Device d1 = new Device("13232291350", "127869187640710");
		d1.setId(new Long(1));
		d1.setLocation((Point) GISUtils.wktToGeometry(String.format("POINT (%.10f %.10f)",34.155028, -118.459927)));
		
		Device d2 = new Device("18184459084", "436198643284772");
		d2.setId(new Long(2));
		d2.setLocation((Point) GISUtils.wktToGeometry(String.format("POINT (%.10f %.10f)",34.1551789, -118.459282)));
		
		Device d3 = new Device("18184459085", "789641873649218");
		d3.setId(new Long(3));
		d3.setLocation((Point) GISUtils.wktToGeometry(String.format("POINT (%.10f %.10f)",34.15533, -118.459927)));
		
		List<Device> devices = new ArrayList<Device>();
		devices.add(d1);
		devices.add(d2);
		devices.add(d3);
		return devices;
	}
}
