package com.headsup.svc.stompws;

public interface ListenerWSNetwork {
	public void onState(int state);
}
