package com.headsup.svc.stompws;

import java.util.Map;

public interface ListenerSubscription {
	public void onMessage(Map<String, String> headers, String body);
}
