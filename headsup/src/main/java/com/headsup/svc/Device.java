package com.headsup.svc;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.validation.constraints.Digits;

import org.apache.commons.lang3.builder.CompareToBuilder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.vividsolutions.jts.geom.Point;

@Entity
@Component
@Scope("prototype")
@JsonIgnoreProperties(ignoreUnknown = true)
//@Access(AccessType.PROPERTY)
//"insert into Device(imei, location, phoneNumber, id) VALUES(?, Point(?, ?), ?, ?)"
//@SQLInsert( sql="INSERT INTO Device(imei, location, phoneNumber, id) VALUES(?, pointfromwkb(?), ?, ?)")
//@SQLUpdate( sql="UPDATE Device SET imei = ?, location = ?, phoneNumber = ? WHERE id = ?")
//@NamedNativeQuery(name="device", query="select * from Device where xml:id= ?", resultClass = Device.class)
public class Device implements Comparable<Device>, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 241797797204177456L;

	@Id
	//@GeneratedValue(strategy=GenerationType.AUTO)
	@GeneratedValue(generator="increment")
    @GenericGenerator(name="increment", strategy = "increment")
	private Long id;
	
	private String phoneNumber;
	 
	private String imei;
	
//	@OneToOne(cascade=CascadeType.ALL)
//	@JoinColumn(name="location_id")
//	private Location location;
	
//	@Column(columnDefinition="Point", nullable = false)
	//@Column(columnDefinition = "geometry(Point,4326)")
	//@Column(columnDefinition = "com.vividsolutions.jts.geom.Geometry")
	//@Type(type="org.hibernate.spatial.JTSGeometryType")
	//
	
	/*
	 	TINYBLOB   :     maximum length of 255 bytes  
		BLOB       :     maximum length of 65,535 bytes  
		MEDIUMBLOB :     maximum length of 16,777,215 bytes  
		LONGBLOB   :     maximum length of 4,294,967,295 bytes (<-@Lob)
		
		0        < length <=      255  -->  `TINYBLOB`
     	255      < length <=    65535  -->  `BLOB`
   		65535    < length <= 16777215  -->  `MEDIUMBLOB`
		16777215 < length <=    2³¹-1  -->  `LONGBLOB`
	 */
	//@Column(length=65535, columnDefinition = "geometry("+location.getX()+","+location.getY()+")")
	//@Column(columnDefinition="POINT", nullable = false)
	//@Type(type = "org.hibernate.spatial.JTSGeometryType")
	//@Column(length=65535)
	@JsonIgnore
	private Point location;
	
	@Transient
	@Digits(integer=3, fraction=9)
	private Double latitude;
	
	@Transient
	@Digits(integer=3, fraction=9)
	private Double longitude;
	
	Device() {}
	
	public Device(final String phoneNumber, final String imei) {
		setPhoneNumber(phoneNumber);
		setImei(imei);
	}

	public Long getId() {
		return id;
	}
	
	public void setId(final Long id) {
		this.id = id;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(final String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(final String imei) {
		this.imei = imei;
	}
	
	//@Type(type="org.hibernate.spatial.JTSGeometryType")
	//@Column(columnDefinition="POINT")
	public Point getLocation() {
		return location;
	}

	public void setLocation(final Point location) {
		this.location = location;
	}
	
	public Double getLatitude() {
		return location.getX();
	}
	
	public Double getLongitude() {
		return location.getY();
	}

	@Override
	public int compareTo(Device other) {
		if (this == other) {
			return 0;
		}
		if (other == null) {
            return 0;
		}
		
		return new CompareToBuilder()
			.append(getPhoneNumber(), other.getPhoneNumber())
			.toComparison();
	}
	
	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}
		if (object == null) {
            return false;
		}
		if (!(object instanceof Device)) {
			return false;
		}

		final Device other = (Device) object;

		return new EqualsBuilder()
			.append(getId(), other.getId())
			.isEquals();
	}
	
	@Override
	public int hashCode() {
		return new HashCodeBuilder()
			.append(getId())
			.toHashCode();
	}
}
