package com.headsup.svc.repository;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.spatial.criterion.SpatialRestrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.headsup.svc.Device;
import com.headsup.svc.utils.DataUtils;
import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.geom.Geometry;

@Repository
public class DeviceRepository implements IDeviceRepository {

	private static final Logger logger = LoggerFactory.getLogger(DeviceRepository.class);
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public Device getDevice(Long id) {
		Session session = sessionFactory.getCurrentSession();
		final Criteria criteria = session.createCriteria(Device.class)
				.add(Restrictions.eq("id", id));
		return (Device) criteria.uniqueResult();
	}
	
	public Device getDevice(final String phoneNumber, final String imei) {
		Session session = sessionFactory.getCurrentSession();
		final Criteria criteria = session.createCriteria(Device.class)
				.add(Restrictions.eq("phoneNumber", phoneNumber))
				.add(Restrictions.eq("imei", imei));
		return (Device) criteria.uniqueResult();
	}
	
	public void update(final Device device) {
		Session session = sessionFactory.getCurrentSession();
		session.saveOrUpdate(device);
	}

	/*
	 *
	 * as of 7/30/2015 DB function 'dwithin' not supported by this dialect
	 * 
	@SuppressWarnings("unchecked")
	public List<Device> getWithinDistanceOf(final Device device, final Double radius) {
		Session session = sessionFactory.getCurrentSession();
		final Criteria criteria = session.createCriteria(Device.class)
				.add(Restrictions.ne("id", device.getId()))
				.add(SpatialRestrictions.distanceWithin("location", device.getLocation(), radius));
		return (List<Device>) criteria.list();
	}
	*/
	@SuppressWarnings("unchecked")
	public List<Device> getWithinEnvelope(final Device device, final Envelope envelope, final int SRID) {
		Session session = sessionFactory.getCurrentSession();
		final Criteria criteria = session.createCriteria(Device.class)
				.add(Restrictions.ne("id", device.getId()))
				.add(SpatialRestrictions.filter("location", envelope, SRID));
		return (List<Device>) criteria.list();
		//return DataUtils.mockDevices();
	}
	
	@SuppressWarnings("unchecked")
	public List<Device> getWithin(final Device device, final Geometry geometry) {
		Session session = sessionFactory.getCurrentSession();
		final Criteria criteria = session.createCriteria(Device.class)
				.add(Restrictions.ne("id", device.getId()))
				.add(SpatialRestrictions.within("location", geometry));
		return (List<Device>) criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Device> getContains(final Device device, final Geometry geometry) {
		Session session = sessionFactory.getCurrentSession();
		final Criteria criteria = session.createCriteria(Device.class)
				.add(Restrictions.ne("id", device.getId()))
				.add(SpatialRestrictions.filter("location", geometry));
		return (List<Device>) criteria.list();
	}
}
