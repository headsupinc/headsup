package com.headsup.svc.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.client.HttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.converter.StringMessageConverter;
import org.springframework.messaging.simp.stomp.StompSessionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.socket.WebSocketHttpHeaders;
import org.springframework.web.socket.client.WebSocketClient;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;
import org.springframework.web.socket.sockjs.client.RestTemplateXhrTransport;
import org.springframework.web.socket.sockjs.client.SockJsClient;
import org.springframework.web.socket.sockjs.client.Transport;
import org.springframework.web.socket.sockjs.client.WebSocketTransport;

import com.headsup.svc.socket.server.support.MyHandler;
import com.headsup.svc.socket.server.support.MySessionHandler;

@RestController
//@RequestMapping("/message/{phoneNumber}")
@RequestMapping("/message/")
public class MessageController {

	private static final Logger logger = LoggerFactory.getLogger(MessageController.class);
	
	private static SockJsClient sockJsClient;
	private final static WebSocketHttpHeaders headers = new WebSocketHttpHeaders();
	
//	@Autowired
//	private CsrfController csrfController;
	//@Autowired
	//private SimpMessagingTemplate template;
	
	//@RequestMapping(value = "/send/{latitude}/{longitude}/{radius}", method = RequestMethod.POST)
	//@RequestMapping(path="/greetings", method=RequestMethod.POST)
	/*
	public void send(String message) {
		//template.convertAndSend("/topic/send", message);
		
		template.convertAndSendToUser(
				"vadim", "/queue/position-updates", "trade results");
	}
	*/
	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public void register() {
		/*
		StompSessionHandler handler = new StompSessionHandlerAdapter() {

		    @Override
		    public void afterConnected(StompSession session, StompHeaders connectedHeaders) {
		        String sessionId = session.getSessionId();
				
				session.subscribe("/queue/position-updates", new StompFrameHandler() {

				    @Override
				    public Type getPayloadType(StompHeaders headers) {
				        return String.class;
				    }

				    @Override
				    public void handleFrame(StompHeaders headers, Object payload) {
				    	String p = payload.toString();
				    }

				});
				
				session.send("/app/trade", "payload");
				session.send("/topic/trade", "payload");
				session.send("/app/update", "payload2");
				session.send("/topic/update", "payload2");
		    }
		};
		*/
		//
		/*
		List<Transport> transports = new ArrayList<>();
		transports.add(new WebSocketTransport(new StandardWebSocketClient()));
		RestTemplateXhrTransport xhrTransport = new RestTemplateXhrTransport(new RestTemplate());
		//xhrTransport.setRequestHeaders(headers);
		transports.add(xhrTransport);

		sockJsClient = new SockJsClient(transports);
		//headers.keySet().iterator().
		//sockJsClient.setHttpHeaderNames();
		
		//WebSocketClient transport = new StandardWebSocketClient();
		//WebSocketStompClient stompClient = new WebSocketStompClient(transport);
		WebSocketStompClient stompClient = new WebSocketStompClient(sockJsClient);
		stompClient.setMessageConverter(new StringMessageConverter());
		//stompClient.setMessageConverter(new MappingJackson2MessageConverter());
		//stompClient.setTaskScheduler(taskScheduler); // for heartbeats, receipts
		String url = "ws://localhost:8080/portfolio";
		StompSessionHandler handler = new MySessionHandler();
		//headers.add(CsrfToken.class.getName(), "13234287437");
		stompClient.connect(url, headers, handler);
		*/
		headers.add("imei", "vadim");
		WebSocketClient transport = new StandardWebSocketClient();
		WebSocketStompClient stompClient = new WebSocketStompClient(transport);
		stompClient.setMessageConverter(new StringMessageConverter());
		String url = "ws://localhost:8080/portfolio";
		StompSessionHandler handler = new MySessionHandler();
		stompClient.connect(url, headers, handler);
	}
	
	@RequestMapping(value = "/handle", method = RequestMethod.GET)
	public void handle() {
		
		
		List<Transport> transports = new ArrayList<>(2);
		transports.add(new WebSocketTransport(new StandardWebSocketClient()));
		transports.add(new RestTemplateXhrTransport());

		SockJsClient sockJsClient = new SockJsClient(transports);
		sockJsClient.doHandshake(new MyHandler(), "ws://localhost:8080/handle");
	}
}
