package com.headsup.svc.controller;

import java.math.BigDecimal;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mobile.device.DevicePlatform;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.headsup.svc.Device;
import com.headsup.svc.resource.DevicesResource;
import com.headsup.svc.service.IDeviceService;
import com.headsup.svc.utils.GISUtils;
import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.Point;

@RestController
@RequestMapping("/device/{phoneNumber}/{imei}")
public class DeviceController {

	private static final Logger logger = LoggerFactory.getLogger(DeviceController.class);
	
	@Autowired
	private IDeviceService deviceService;
	
	@RequestMapping(value = "/update/{latitude}/{longitude}/{latitude1}/{longitude1}/{latitude2}/{longitude2}/", method = RequestMethod.GET)
	public List<Device> update(org.springframework.mobile.device.Device client, @PathVariable String phoneNumber, @PathVariable String imei, @PathVariable BigDecimal latitude, @PathVariable BigDecimal longitude, @PathVariable BigDecimal latitude1, @PathVariable BigDecimal longitude1, @PathVariable BigDecimal latitude2, @PathVariable BigDecimal longitude2) {
	//public ResponseEntity<DevicesResource> update(org.springframework.mobile.device.Device client, @PathVariable String phoneNumber, @PathVariable String imei, @PathVariable BigDecimal latitude, @PathVariable BigDecimal longitude, @PathVariable Double radius) {
	 //public DevicesResource update(@PathVariable String phoneNumber, @PathVariable String imei, @PathVariable BigDecimal latitude, @PathVariable BigDecimal longitude, @PathVariable Double radius) {
		if (client.isMobile() && DevicePlatform.ANDROID.equals(client.getDevicePlatform())) {
			logger.info("mobile client");
		}
		Device device = deviceService.getDevice(phoneNumber, imei);
		if (device == null) {
			device = new Device(phoneNumber, imei);
		}
		
		String wktPoint = String.format("POINT (%.10f %.10f)",latitude, longitude);
		Geometry geom =  GISUtils.wktToGeometry(wktPoint);
		device.setLocation((Point) geom);
		deviceService.update(device);
		/*
		Device d = deviceService.getDevice(device.getId());
		*/
		//Envelope envelope = geom.getEnvelopeInternal();
		//envelope.expandBy(radius);//111
		
		Envelope envelope = new Envelope(latitude1.doubleValue(), latitude2.doubleValue(), longitude1.doubleValue(), longitude2.doubleValue());
		
		List<Device> devices = deviceService.getWithinEnvelope(device, envelope, geom.getSRID());
		/*
		boolean b = envelope.contains(d.getLocation().getX(), d.getLocation().getY());
		
		Polygon polygon = EnvelopeAdapter.toPolygon(envelope, geom.getSRID());//6371
		List<Device> devices = deviceService.getWithin(device, polygon);
		*/
		
		//devices = deviceService.getWithinDistanceOf(device, radius); // DB function 'dwithin' not supported by this dialect
		
		/*
		GeometricShapeFactory shapeFactory = new GeometricShapeFactory();
        shapeFactory.setNumPoints(32);
        shapeFactory.setCentre(new Coordinate(d.getLocation().getX(), d.getLocation().getY()));
        shapeFactory.setSize(0.5);
 
        Geometry aroundPointShape = shapeFactory.createCircle();
        devices = deviceService.getWithin(device, aroundPointShape);
		*/
		//return new ResponseEntity<DevicesResource>(new DevicesResource(devices), HttpStatus.OK);
		return devices;
    }
	
	
}