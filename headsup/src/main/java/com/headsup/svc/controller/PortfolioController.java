package com.headsup.svc.controller;

import java.security.Principal;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.stereotype.Controller;

@Controller
public class PortfolioController {

    @MessageMapping("/trade")
    @SendToUser(destinations="/queue/position-updates", broadcast=false)
    public String executeTrade(String payload, Principal principal) {
    	String name = principal.getName();
    	return payload + " " + name;
    }
    
    @MessageMapping("/update")
    @SendToUser("/queue/position-updates")
    public String executeTrade(String payload) {
    	return payload;
    }
}
