package com.headsup.svc.controller;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.headsup.svc.stompws.ListenerSubscription;
import com.headsup.svc.stompws.ListenerWSNetwork;
import com.headsup.svc.stompws.Stomp;
import com.headsup.svc.stompws.Subscription;

@RestController
@RequestMapping("/weberknecht/")
public class WeberknechtController {

	private static final Logger logger = LoggerFactory.getLogger(WeberknechtController.class);
	
	@RequestMapping(value = "/stomp", method = RequestMethod.GET)
	public void weberknecht() {
		try {
			Map<String,String> headersSetup = new HashMap<String,String>();
			headersSetup.put("imei", "vadim");
//			headersSetup.put("session", "123");
//			headersSetup.put("JSESSIONID", "456");
//			headersSetup.put("id", "789");
	        Stomp stomp = new Stomp("ws://127.0.0.1:8080/portfolio", headersSetup, new ListenerWSNetwork() {
	            @Override
	            public void onState(int state) {
	            	int s = state;
	            }
	        });
	        stomp.connect();
	        ///user/queue/position-updates
	        stomp.subscribe(new Subscription("/user/vadim/queue/position-updates", new ListenerSubscription() {
	            @Override
	            public void onMessage(Map<String, String> headers, String body) {
	            	String msg = body;
	            	stomp.disconnect();
	            }
	        }));
//	        Map<String,String> headers = new HashMap<String, String>();
//	        headers.put("tagetUser", "vadim");
	        stomp.send("/app/trade", null, "hello");
		} catch (Exception e) {
			String msg = e.getMessage();
		}
	}
}
