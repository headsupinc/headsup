package com.headsup.svc;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.validation.constraints.Digits;

import org.apache.commons.lang3.builder.CompareToBuilder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

//@Entity
//@Component
//@Scope("prototype")
public class Location implements Comparable<Location>, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1296591741502377246L;

//	@Id
//	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Digits(integer=3, fraction=9)
	private BigDecimal latitude;
	
	@Digits(integer=3, fraction=9)
	private BigDecimal longitude;
	
	Location() {}
	
	public Location(final BigDecimal latitude, final BigDecimal longitude) {
		setLatitude(latitude);
		setLongitude(longitude);
	}

	public Long getId() {
		return id;
	}
	
	public void setId(final Long id) {
		this.id = id;
	}
	
	public BigDecimal getLatitude() {
		return latitude;
	}

	public void setLatitude(final BigDecimal latitude) {
		this.latitude = latitude;
	}

	public BigDecimal getLongitude() {
		return longitude;
	}

	public void setLongitude(final BigDecimal longitude) {
		this.longitude = longitude;
	}
	
	@Override
	public int compareTo(Location other) {
		if (this == other) {
			return 0;
		}
		if (other == null) {
            return 0;
		}
		
		return new CompareToBuilder()
			.append(getLatitude(), other.getLatitude())
			.append(getLongitude(), other.getLongitude())
			.toComparison();
	}
	
	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}
		if (object == null) {
            return false;
		}
		if (!(object instanceof Location)) {
			return false;
		}

		final Location other = (Location) object;

		return new EqualsBuilder()
			.append(getId(), other.getId())
			.isEquals();
	}
	
	@Override
	public int hashCode() {
		return new HashCodeBuilder()
			.append(getId())
			.toHashCode();
	}
}
