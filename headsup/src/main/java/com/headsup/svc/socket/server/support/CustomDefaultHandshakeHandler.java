package com.headsup.svc.socket.server.support;

import java.security.Principal;
import java.util.Map;

import org.springframework.http.server.ServerHttpRequest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.server.support.DefaultHandshakeHandler;

public class CustomDefaultHandshakeHandler extends DefaultHandshakeHandler {

	@Override
	protected Principal determineUser(ServerHttpRequest request, WebSocketHandler wsHandler,
			Map<String, Object> attributes) {

		Principal principal = request.getPrincipal();
		if (principal == null) {
			principal = new Principal() {
				
				@Override
				public String getName() {
					// TODO Auto-generated method stub
					return request.getHeaders().getFirst("imei");
				}
			};
		}
		
		//SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(principal, null, AuthorityUtils.createAuthorityList("ROLE_USER")));
		
		return principal;
	}
}
