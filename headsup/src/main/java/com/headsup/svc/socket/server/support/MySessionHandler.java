package com.headsup.svc.socket.server.support;

import java.lang.reflect.Type;

import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompFrameHandler;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;

public class MySessionHandler extends StompSessionHandlerAdapter {

    @Override
    public void afterConnected(StompSession session, StompHeaders connectedHeaders) {
        String sessionId = session.getSessionId();
		
		session.subscribe("/user/vadim/queue/position-updates", new StompFrameHandler() { ///user/queue/position-updates

		    @Override
		    public Type getPayloadType(StompHeaders headers) {
		        return String.class;
		    }

		    @Override
		    public void handleFrame(StompHeaders headers, Object payload) {
		    	String p = payload.toString();
		    }

		});
		
		session.send("/app/trade", "payload");
		//session.send("/topic/trade", "payload");
		//session.send("/app/update", "payload2");
		//session.send("/topic/update", "payload2");
    }
    
    @Override
	public void handleFrame(StompHeaders headers, Object payload) {
    	String p = payload.toString();
	}

	/**
	 * This implementation is empty.
	 */
	@Override
	public void handleException(StompSession session, StompCommand command, StompHeaders headers,
			byte[] payload, Throwable exception) {
		String e = exception.getMessage();
	}

	/**
	 * This implementation is empty.
	 */
	@Override
	public void handleTransportError(StompSession session, Throwable exception) {
		String e = exception.getMessage();
	}
}
