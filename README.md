### What is this repository for? ###

* HeadsUp web service layer repository

* Version 1.0

### How do I get set up? ###

* Dependencies

    1. Download and Install Java 8 JDK:

        http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html

    2. Download and Install Git:

        http://git-scm.com/download/win

* Summary of set up

    1. Download and Install MySQL 5.7+:

        http://dev.mysql.com/downloads/windows/installer/

    2. Download and Install RabbitMQ:

        http://www.rabbitmq.com/install-windows.html

    3. Download and Install SpringSource Tool Suite (SSTS):

        https://spring.io/tools

* Configuration

    1. Java 8 JDK Installation and Configuration:

        default installation (make note of the JDK installation folder location)

    2. Git Installation and Configuration:

        https://confluence.atlassian.com/bitbucket/set-up-git-744723531.html

    3. MySQL 5.7+ Installation and Configuration:
       
        specify port `3307` during installation
        
        make a note of the root username and password

    4. RabbitMQ Installation and Configuration:

        http://www.rabbitmq.com/stomp.html

        1. Launch the RabbitMQ Command Prompt

        2. Execute the following command: `rabbitmq-plugins enable rabbitmq_stomp`

    5. SpringSource Tool Suite (SSTS) Installation and Configuration: 

        default installation https://spring.io/tools

* Database configuration

    1. Launch MySQL Workbench

    2. Click on + icon to add/setup a new DB connection:

        connection name: `localhost`

        port: `3307`

        username: `root`

        password: `root password you specified during MySQL installation and configuration`

    3. Click on the newly created connection to open it.

    4. Add new user account under Users and Privileges:

        login name: `headsup`

        limit to hosts matching: `localhost`

        password: `Gavr1lk@`

        administrative roles: `DBA`

        schema privileges: schema`headsup` privileges `SELECT, INSERT, UPDATE, DELETE, EXECUTE, SHOW VIEW`

    5. Right-click under SCHEMAS and create a new schema with name: `headsup`

* SpringSource Tool Suite (SSTS) Project configuration

    1. Launch SSTS

    2. Right-click in Package Explorer then `Import...`

    3. Select `Projects from Git` under `Git` menu item then `Next >`

    4. Select `Existing local repository` then `Next >`

    5. Select `headsup` then `Next >`

    6. Select `Import existing Eclipse projects` then `Next >`

    7. Check `headsup` then `Finish`

* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin ** Vadim Klochko **

* Other community or team contact